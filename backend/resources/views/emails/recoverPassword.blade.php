<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
 <title>Studio96 - recover password</title>
</head>
<body>
<p>Hi, did you want to reset your password?</p>


Someone (hopefully you) has asked us to reset the password for your admin account.
Please click the link below to do so. If you didn't request this password reset, you can go ahead and ignore this email!

<a href="https://studio96-architektura.pl/login/recover-password/token?hash={{$test_message}}">https://studio96-architektura.pl/login/recover-password/token?hash={{$test_message}}</a>

</body>
</html> 