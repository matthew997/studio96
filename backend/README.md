###

```
composer install

npm install

cp .env.example .env

php artisan key:generate

php artisan migrate

php artisan db:seed

php artisan passport:keys

php artisan passport:client --personal
```

###
