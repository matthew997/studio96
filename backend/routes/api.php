<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('user', [UserController::class, 'show']);
// Route::get('user', [UserController::class, 'show']);

Route::group(['middleware' => ['cors']], function () {
    Route::post("login", [UserController::class, 'login']);
    Route::post("recover-password",  [UserController::class, 'recoverPasswordSendEmail']);
    Route::post("recover-password/{token}",  [UserController::class, 'recoverPassword']);
    Route::get("categories",  [CategoryController::class, 'index']);
    Route::get("categories/{id}",  [CategoryController::class, 'show']);
    Route::get("portfolios",  [PortfolioController::class, 'index']);
    Route::get("portfolios/{id}",  [PortfolioController::class, 'show']);
    Route::get("home-page",  [HomePageController::class, 'index']);
    Route::post("contact",  [ContactController::class, 'sendEmail']);
    Route::get("contents/{slug}",  [ContentController::class, 'show']);
    Route::get("seo",  [PortfolioController::class, 'showSlugs']);
});

Route::group(['middleware' => ['auth:api', 'cors']], function () {
    Route::get("me", [UserController::class, 'me']);
    Route::post("categories",  [CategoryController::class, 'create']);
    Route::put("categories/{id}",  [CategoryController::class, 'update']);
    Route::delete("categories/{id}",  [CategoryController::class, 'delete']);
    Route::post("portfolios",  [PortfolioController::class, 'create']);
    Route::delete("portfolios/{id}",  [PortfolioController::class, 'delete']);
    Route::put("portfolios/{id}",  [PortfolioController::class, 'update']);
    Route::post("images",  [GalleryController::class, 'create']);
    Route::get("images",  [GalleryController::class, 'index']);
    Route::get("images/{id}",  [GalleryController::class, 'show']);
    Route::delete("images/{id}",  [GalleryController::class, 'delete']);
    Route::post("home-page",  [HomePageController::class, 'create']);
    Route::put("home-page/{id}",  [HomePageController::class, 'update']);
    Route::delete("home-page/{id}",  [HomePageController::class, 'delete']);
    Route::post("contents",  [ContentController::class, 'create']);
    Route::put("contents/{slug}",  [ContentController::class, 'update']);
    Route::get("contents",  [ContentController::class, 'index']);
    Route::delete("contents/{slug}",  [ContentController::class, 'delete']);
});
