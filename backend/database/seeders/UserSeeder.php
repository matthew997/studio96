<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('laravel_users')->insert([
            'name' => 'Mateusz Rozkosz',
            'email' => 'mateuszrozkosz97@gmail.com',
            'password' => Hash::make(Str::random(15)),
        ]);
    }
}
