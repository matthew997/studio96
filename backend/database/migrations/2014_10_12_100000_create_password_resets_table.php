<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_password_resets', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id') -> nullable(false);
            $table->foreign('user_id')->references('id')->on('laravel_users');
            $table->string('expiration') -> nullable(false);
            $table->string('hash') -> nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_password_resets');
    }
}
