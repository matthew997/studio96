<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_portfolios', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->string('slug')->unique();
            $table->unsignedBigInteger('categorie_id') -> nullable(false);
            $table->foreign('categorie_id')->references('id')->on('laravel_categories');
            $table->string('name');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_portfolios');
    }
}
