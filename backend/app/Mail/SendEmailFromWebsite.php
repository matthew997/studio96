<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailFromWebsite extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     *
     */

    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'biuro@studio96-architektura.pl';
        $name = 'kontakt@studio96-architektura.pl';

        return $this->from($this->data['from'], $name)
        ->view('emails.emailFromWebsite')
                    ->subject($this->data['subject'])
                    ->with([ 'test_message' => $this->data['message'],
                            'from' => $this->data['from'],
                            'subject' => $this->data['subject']    
                    ]);
    
    }
}
