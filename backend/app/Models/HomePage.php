<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    use HasFactory;

    protected $table = 'laravel_home_page';

    protected $fillable = [
        'image_position',
        'name',
        'image',
    ];
}
