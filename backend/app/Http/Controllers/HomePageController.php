<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\HomePage;
use Image;

class HomePageController extends Controller
{
    public function index(Request $request){
     
        return  HomePage::orderBy('image_position', 'ASC')->paginate(20);
    }


    public function show(Request $request){}

    public function create(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name'=>'required',
            'image' => 'required|image:jpeg,png,jpg,gif,svg'
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);
        }

        $latestHomePageImage = HomePage::latest()->first();

        $imagePosition = 1;

        if($latestHomePageImage){
            $imagePosition = $latestHomePageImage->image_position + 1;
        }

        $destinationPath = '../laravel/images';
        $thumbnailDestinationPath = '../laravel/thumbnail';

        $image = $request->file('image');
   
        $image_name = uniqid() . '.' . $image->getClientOriginalExtension();
        $resize_image = Image::make($image->getRealPath());

        $resize_image->resize(1000, 700, function($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $image_name);

        $resize_image->resize(500, 500, function($constraint){
            $constraint->aspectRatio();
        })->save($thumbnailDestinationPath . '/' . $image_name);

        $homePage = HomePage::create([
            'name' => $request->name,
            'image' => $image_name,
            'image_position' => $imagePosition
        ]);

        return  $homePage;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name'=>'required',
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);
        }

        $homePage = HomePage::where('id',$request->id)->first();

        if(!$homePage){
            return response()->json(['statusCode'=>404,'success'=>false], 404);
        }

        $homePage->name = $request->name;
    
        $homePage->save();

        return $homePage;
    }

    public function delete(Request $request){

        $homePage = HomePage::where('id',$request->id)->first();
    
        if(!$homePage){
            return response()->json(['statusCode'=>404], 404);
        }

        $homePage->delete();
    
        return response()->json(['statusCode'=>204], 204); 
        }

}
