<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Portfolio;
use App\Models\Category;
use Image;


class PortfolioController extends Controller
{
    public function index(Request $request)
    {

        $random = $request->random;
        $categorie_id = $request->categorie_id;

        if ($random) {
            return Portfolio::inRandomOrder()->limit(10)->get();
        }

        if (!$categorie_id) {
            return Portfolio::orderBy('updated_at', 'desc')
                ->paginate(10);
        }

        return Portfolio::orderBy('updated_at', 'desc')
            ->where('categorie_id', $categorie_id)
            ->orWhere('slug', $categorie_id)
            ->paginate(10);
    }

    public function show(Request $request)
    {
        $portfolio = Portfolio::where('id', $request->id)
            ->orWhere('slug', $request->id)->first();

        if (!$portfolio) {
            return response()->json(['statusCode' => 404], 404);
        }

        $gallery = $portfolio->gallery;

        if (!$gallery) {
            return response()->json(['statusCode' => 404], 404);
        }

        return $portfolio;
    }

    public function showSlugs(Request $request)
    {
        return Portfolio::pluck('slug');
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'categorie_id' => 'required',
            'name' => 'required',
            'image' => 'required|image:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode' => 400, 'success' => false, 'message' => $message], 400);
        }

        $slug = $this->slugify($request->name);

        $portfolio = Portfolio::where("slug", $slug)->first();

        if ($portfolio) {
            $slug = $slug . '-' . time();
        }

        $category = Category::where("id", $request->categorie_id)->first();

        if (!$category) {
            $message = "category not found";
            return response()->json(['statusCode' => 400, 'message' => $message], 400);
        }

        $image = $request->file('image');

        $image_name = time() . '.' . $image->getClientOriginalExtension();



        $destinationPath = '../laravel/thumbnail';

        $resize_image = Image::make($image->getRealPath());

        $resize_image->resize(150, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $image_name);

        $destinationPath = '../laravel/images';

        $image->move($destinationPath, $image_name);

        $portfolio = Portfolio::create([
            'categorie_id' => $request->categorie_id,
            'name' => $request->name,
            'image' => $image_name,
            'slug' => $slug
        ]);

        return $portfolio;
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'categorie_id' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode' => 400, 'success' => false, 'message' => $message], 400);
        }

        $portfolio = Portfolio::where('id', $request->id)->first();

        if (!$portfolio) {
            return response()->json(['statusCode' => 404, 'success' => false], 404);
        }

        $portfolio->name = $request->name;
        $portfolio->categorie_id = $request->categorie_id;

        $portfolio->save();

        return $portfolio;
    }


    public function delete(Request $request)
    {
        $portfolio = Portfolio::where('id', $request->id)->first();

        if (!$portfolio) {
            return response()->json(['statusCode' => 404], 404);
        }

        $portfolio->delete();

        return response()->json(['statusCode' => 204], 204);
    }

    public static function slugify($text)
    {

        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        $text = preg_replace('~[^-\w]+~', '', $text);

        $text = trim($text, '-');

        $text = preg_replace('~-+~', '-', $text);

        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
