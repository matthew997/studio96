<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\Portfolio;
use Image;


class GalleryController extends Controller
{

    public function index(Request $request)
    {

        return  Gallery::orderBy('updated_at', 'desc')->paginate(5);
    }

    public function show(Request $request)
    {
        $image = Gallery::where('portfolio_id', $request->id)->get();

        if (!$image) {
            return response()->json(['statusCode' => 404], 404);
        }

        return $image;
    }


    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'portfolio_id' => 'required',
            'images' => 'required'
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode' => 400, 'success' => false, 'message' => $message], 400);
        }

        $portfolio = Portfolio::where("id", $request->portfolio_id)->first();

        if (!$portfolio) {
            $message = "portfolio not found";
            return response()->json(['statusCode' => 400, 'message' => $message], 400);
        }

        $destinationPath = '../laravel/images';
        $thumbnailDestinationPath = '../laravel/thumbnail';
        $images = [];
        $i = 0;

        if ($request->has('images')) {

            foreach ($request->file('images') as $image) {
                $image_name = uniqid() . '-' . uniqid() . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
                $resize_image = Image::make($image->getRealPath());

                $resize_image->resize(1000, 700, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $image_name);

                $resize_image->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbnailDestinationPath . '/' . $image_name);


                $galleryImage = Gallery::create([
                    'portfolio_id' => $request->portfolio_id,
                    'name' => $_FILES['images']['name'][$i],
                    'image' => $image_name,
                ]);

                $i += 1;

                array_push($images, $galleryImage);
            }
        }

        return $images;
    }

    public function resize(Request $request)
    {
        $width = $request->w;
        $height = $request->h;

        if (!$request->w) {
            $width = 300;
        }

        if (!$request->h) {
            $width = 300;
        }

        $path = '../laravel/images/' . $request->name;
        $filename = basename($path);

        if (!$filename) {
            return response()->json(['statusCode' => 404, 'message' => "not found"], 404);
        }

        $img = Image::make($path);

        header('Content-Type: image');

        $img->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $img->response();
    }


    public function delete(Request $request)
    {
        $portfolio = Gallery::where('id', $request->id)->first();

        if (!$portfolio) {
            return response()->json(['statusCode' => 404], 404);
        }

        $portfolio->delete();

        return response()->json(['statusCode' => 204], 204);
    }
}
