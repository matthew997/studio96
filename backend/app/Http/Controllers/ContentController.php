<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function index(Request $request){
        
        $content = Content::all();

        return $content;
    }

    public function show(Request $request){
        $slug = $request->slug;
        
        $content = Content::where('slug',$slug)->first();

        if(!$content){
            return response()->json(['statusCode'=>404], 404);
        }

        return $content->content;
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [ 
            'slug'=>'required',
            'content' => 'required'
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);
        }

        $content = Content::create([
            'slug' => $this->slugify($request->slug),
            'content' => $request->content,
        ]);

        if(!$content){
            return response()->json(['statusCode'=>400,'success'=>false], 400);
        }
        
        return $content;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'content' => 'required'
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);
        }

        $content = Content::where('slug', $request->slug)->first();

        $content->content = $request->content;

        $content->save();


        if(!$content){
            return response()->json(['statusCode'=>400,'success'=>false], 400);
        }
        
        return $content;  
    }

    public function delete(Request $request){
        $content = Content::where('slug',$request->slug)->first();
        
        if(!$content){
            return response()->json(['statusCode'=>404], 404);
        }

        $content->delete();
    
        return response()->json(['statusCode'=>204], 204); 
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
