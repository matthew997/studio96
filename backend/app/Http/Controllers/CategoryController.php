<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Portfolio;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->all) {
            return Category::orderBy('updated_at', 'desc')->get();
        }
        return Category::orderBy('updated_at', 'desc')->paginate(5);
    }

    public function show(Request $request)
    {
        $category = Category::where('id', $request->id)
            ->orWhere('slug', $request->id)
            ->first();

        if (!$category) {
            return response()->json(['statusCode' => 404], 404);
        }

        return $category;
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['statusCode' => 400, 'success' => false, 'message' => $message], 400);
        }

        $slug = $this->slugify($request->name);

        $category = Category::where('slug', 'like', $slug . '%')->get();

        if ($category) {
            return response()->json(['statusCode' => 422, 'success' => false, 'message' => 'Already exist'], 422);
        }

        $createdCategory = Category::create([
            'slug' => $slug,
            'name' => $request->name,
        ]);

        return $createdCategory;
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['statusCode' => 400, 'success' => false, 'message' => $message], 400);
        }

        $category = Category::where("id", $request->id)->first();

        if (!$category) {
            return response()->json(['statusCode' => 404], 404);
        }

        $category->name = $request->name;

        $category->save();


        return $category;
    }


    public function delete(Request $request, $id)
    {

        $category = Category::where("id", $id)->first();

        if (!$category) {
            return response()->json(['statusCode' => 204], 204);
        }

        $category->delete();


        return response()->json(['statusCode' => 204], 204);
    }

    public static function slugify($text)
    {

        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        $text = preg_replace('~[^-\w]+~', '', $text);

        $text = trim($text, '-');

        $text = preg_replace('~-+~', '-', $text);

        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
