<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\Models\PasswordReset;
use App\Models\User;

class UserController extends Controller
{
   
    public function show(){
         $user = Auth::user();

        if(!$user){
            return response('Not Found', 404)
            ->header('Content-Type', 'text/plain');
        }

        return $user;
    }

    private $sucess_status = 200;

    public function login(Request $request) {

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user       =       Auth::user();
            $token      =       $user->createToken('token')->accessToken;

            return response()->json(["status" => $this->sucess_status, "success" => true, "login" => true, "token" => $token, "user" => $user]);
        }
        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! invalid email or password"]);
        }
    }

    public function me() {
        $user = Auth::user();
       
        if(!is_null($user)) {
            return response()->json(["status" => $this->sucess_status, "success" => true, "user" => $user]);
        }
        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! no user found"]);
        }
    }

   public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function recoverPasswordSendEmail(Request $request){
       
        $validator = Validator::make($request->all(), [ 
            'email'=>'required',
         ]);
     
         if ($validator->fails())
         { 
             $message = $validator->errors()->first();
             return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);            
         }

         $user = User::where('email',$request->email) -> first();

         if(!$user){
             return 0;
         }

         $hash = $this->generateRandomString();  

         PasswordReset::create([
            'user_id' => $user->id,
            'hash'=>$hash,
            'expiration'=>time() + 60*60
         ]);
         

         $data = ['message' => $hash, 'subject' => 'Password recovery'];
         Mail::to($user->email)->send(new SendEmail($data));

         return response()->json(['statusCode'=>204,'success'=>true], 204);   

    }

    public function recoverPassword(Request $request, $token){

        $validator = Validator::make($request->all(), [ 
            'password'=>'required',
         ]);

         if ($validator->fails())
         { 
             $message = $validator->errors()->first();
             return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);            
         }

        $passwordReset = PasswordReset::where('hash',$token)->first();

        if(!$passwordReset){
            return response()->json(['statusCode'=>404], 404);
        }

        $now = time() + 60*60;

        if($now <= $passwordReset->expiration){
            return response()->json(['statusCode'=>403], 403);
        }


        $user = User::where('id', $passwordReset->user_id)->first();
        
        $user->makeVisible('password')->toArray();

        $user->password = bcrypt($request->password);

        $user->update();

        $passwordReset::where('user_id', $passwordReset->user_id)->delete();
    }
}

