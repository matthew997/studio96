<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendEmailFromWebsite;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function sendEmail(Request $request){
    
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'email'=>'required',
            'text' => 'required'
         ]);
     
         if ($validator->fails())
         { 
             $message = $validator->errors()->first();
             return response()->json(['statusCode'=>400,'success'=>false,'message'=>$message], 400);            
         }


         $data = ['message' => $request->text, 'subject' => $request->name, 'from'=>$request->email];
         $mail = Mail::to('biuro@studio96-architektura.pl')->send(new SendEmailFromWebsite($data));
    
         return $mail;
    }
}
