const env = require('dotenv').config()

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  ssr: false,

  env: {
    appUrl: env.parsed.APP_URL || 'http://localhost:3000',
    apiBaseUrl: env.parsed.API_BASE_URL || 'http://localhost:3001',
    mediaBaseUrl: env.parsed.MEDIA_BASE_URL || 'http://localhost:3001',
    tinyApiKey:
      env.parsed.TINY_API_KEY ||
      'rsmhwbe3jgi3mhezer07mck8qqlqpgaqw908gk3wsjslxsob',
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: '~plugins/vue-tiny', ssr: false }],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: env.parsed.API_BASE_URL,
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    vendor: ['@tinymce/tinymce-vue'],
    babel: {
      compact: true,
    },
  },

  auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/',
      user: '/',
    },

    strategies: {
      local: {
        token: {
          property: 'token',
          type: 'Bearer',
        },
        endpoints: {
          login: { url: '/login', method: 'post' },
          user: { url: '/me', method: 'get' },
        },
      },
    },
  },
}
