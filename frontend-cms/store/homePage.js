'use strict';

export const defaultState = () => {
    return {
        home: {}
    };
};

export const state = () => defaultState();

export const getters = {
    getHome: state => state.home
};

export const mutations = {
    SET_HOME(state, home) {
        state.home = home;
    },

    DELETE_HOME_INTEM(state, id) {
        const home = state.home;
        const removeIndex = home.data
            .map(function(item) {
                return item.id;
            })
            .indexOf(id);
        state.home.data.splice(removeIndex, 1);
    },

    ADD_HOME_ITEMS(state, data) {
        state.home.data.push(data);
    }
};

export const actions = {
    async getData({ commit }, page = null) {
        const { data } = await this.$axios.get(`/home-page`, {
            params: { page }
        });

        commit('SET_HOME', data);

        return data;
    },

    async deleteItem({ commit }, id) {
        await this.$axios.delete(`/home-page/${id}`);

        commit('DELETE_HOME_INTEM', id);
    },

    async createItem({ commit }, formData) {
        const { data } = await this.$axios.post(`/home-page`, formData, {
            headers: {
                accept: 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': `multipart/form-data; `
            }
        });

        commit('ADD_HOME_ITEMS', data);
    },

    async update({ commit }, { id, formData }) {
        const { data } = await this.$axios.put(`/home-page/${id}`, formData);
        // commit('update', data);
        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
