'use strict';

export const defaultState = () => {
    return {
        
    };
};

export const state = () => defaultState();

export const getters = {
    
};

export const mutations = {
    
};

export const actions = {
 
    async sendEmail({ commit }, params) {
        const data  = await this.$axios.post(`/contact`, params.form);

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
