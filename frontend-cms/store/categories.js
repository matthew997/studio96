'use strict';

export const defaultState = () => {
    return {
        categories: {}
    };
};

export const state = () => defaultState();

export const getters = {
    getCategories: state => state.categories
};

export const mutations = {
    SET_CATEGORIES(state, categories) {
        state.categories = categories;
    },

    DELETE_CATEGORIES(state, id) {
        const categories = state.categories;
        const removeIndex = categories.data
            .map(function(item) {
                return item.id;
            })
            .indexOf(id);
        state.categories.data.splice(removeIndex, 1);
    },

    ADD_CATEGORIES(state, data) {
        state.categories.data.push(data);
    }
};

export const actions = {
    async getData({ commit }, page = null, all = null) {
        const { data } = await this.$axios.get(`/categories`, {
            params: { page, all }
        });

        commit('SET_CATEGORIES', data);

        return data;
    },

    async getAllCategories({ commit }) {
        const all = true;
        const { data } = await this.$axios.get(`/categories`, {
            params: { all }
        });

        commit('SET_CATEGORIES', data);

        return data;
    },

    async deleteItem({ commit }, id) {
        await this.$axios.delete(`/categories/${id}`);

        commit('DELETE_CATEGORIES', id);
    },

    async createItem({ commit }, { name }) {
        const { data } = await this.$axios.post(`/categories`, { name });

        commit('ADD_CATEGORIES', data);
    },

    async updateItem({ commit }, { id, name }) {
        const { data } = await this.$axios.put(`/categories/${id}`, { name });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
