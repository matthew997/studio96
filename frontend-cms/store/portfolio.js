'use strict';

export const defaultState = () => {
    return {
        portfolios: {},
        portfolio: {},
        images: {}
    };
};

export const state = () => defaultState();

export const getters = {
    getPortfolios: state => state.portfolios,
    getPortfolio: state => state.portfolio,
    getGallery: state => state.images
};

export const mutations = {
    SET_PORTFOLIOS(state, portfolios) {
        state.portfolios = portfolios;
    },

    SET_PORTFOLIO(state, portfolio) {
        state.portfolio = portfolio;
    },

    DELETE_PORTFOLIOS(state, id) {
        const portfolios = state.portfolios;
        const removeIndex = portfolios.data
            .map(function(item) {
                return item.id;
            })
            .indexOf(id);
        state.portfolios.data.splice(removeIndex, 1);
    },

    ADD_PORTFOLIOS(state, data) {
        state.portfolios.data.push(data);
    },

    ADD_MORE_PORTFOLIOS(state, portfolio) {
        if (!state.portfolios.data) {
            state.portfolios = portfolio;
        } else {
            state.portfolios.next_page_url = portfolio.next_page_url;

            let temp = state.portfolios.data;
            portfolio.data.map(function(item) {
                temp.push(item);
            });
            state.portfolios.data = [...new Set(portfolio.data)];
        }
    },

    ADD_IMAGES(state, data) {
        state.images = data;
    },

    DELETE_IMAGE_GALLERY(state, id) {
        const images = state.images;
        const removeIndex = images
            .map(function(item) {
                return item.id;
            })
            .indexOf(id);
        state.images.splice(removeIndex, 1);
    },

    UPDATE_IMAGE_GALLERY(state, data) {
        data.map(function(item) {
            state.images.push(item);
        });
    }
};

export const actions = {
    async getData({ commit }, page = null) {
        const { data } = await this.$axios.get(`/portfolios`, {
            params: { page }
        });

        commit('SET_PORTFOLIOS', data);

        return data;
    },

    async getPortfolios({ commit }, { categorie_id, page = null }) {
        const { data } = await this.$axios.get(`/portfolios`, {
            params: { page, categorie_id }
        });

        commit('ADD_MORE_PORTFOLIOS', data);

        return data;
    },

    async getPortfolioById({ commit }, { id }) {
        const { data } = await this.$axios.get(`/portfolios/${id}`);

        commit('SET_PORTFOLIO', data);

        return data;
    },

    async deleteItem({ commit }, id) {
        await this.$axios.delete(`/portfolios/${id}`);

        commit('DELETE_PORTFOLIOS', id);
    },

    async createItem({ commit }, formData) {
        const { data } = await this.$axios.post(`/portfolios`, formData, {
            headers: {
                accept: 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': `multipart/form-data; `
            }
        });

        commit('ADD_PORTFOLIOS', data);

        return data;
    },

    async updatePortfolioName({ commit }, { id, formData }) {
        const { data } = await this.$axios.put(`/portfolios/${id}`, formData);
        // commit('update', data);
        return data;
    },

    async featchGallery({ commit }, portfolio_id) {
        const { data } = await this.$axios.get(`/images/${portfolio_id}`);

        commit('ADD_IMAGES', data);
    },

    async createGallery({ commit }, formData) {
        const { data } = await this.$axios.post(`/images`, formData, {
            headers: {
                accept: 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': `multipart/form-data; `
            }
        });

        commit('UPDATE_IMAGE_GALLERY', data);

        return data;
    },

    async deleteImageFromGallery({ commit }, id) {
        await this.$axios.delete(`/images/${id}`);

        commit('DELETE_IMAGE_GALLERY', id);
    },

    async deletePortfolio({ commit }, id) {
        await this.$axios.delete(`/portfolios/${id}`);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
