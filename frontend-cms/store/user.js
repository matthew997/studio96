'use strict';

export const defaultState = () => {
    return {
        user: null,
   
    };
};

export const state = () => defaultState();

export const getters = {
    getUser: state => state.user,
    getUserDetails: state => state.userDetails,
};

export const mutations = {
    SET_USER(state, user) {
        state.user = user;
    }
};

export const actions = {
    async getData({ commit }, role = null) {
        const { data } = await this.$axios.get(`/me`, { params: { role } });

        commit('SET_USER', data);

        return data;
    },

};

export default {
    state,
    getters,
    mutations,
    actions
};
