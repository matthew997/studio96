import Vue from 'vue'
import { LightGallery } from 'vue-light-gallery'

Vue.component('light-gallery', LightGallery)
