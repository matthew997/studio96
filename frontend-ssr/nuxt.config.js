const env = require('dotenv').config()
const axios = require('axios')

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  mode: 'universal',

  head: {
    script: [
      {
        hid: 'gtm-script1',
        src: 'https://www.googletagmanager.com/gtag/js?id=G-1VDTW3HQKV',
        async: true,
        crossorigin: 'anonymous',
      },
      {
        hid: 'gtm-script2',
        innerHTML: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-1VDTW3HQKV');
        `,
        type: 'text/javascript',
        charset: 'utf-8',
      },
    ],
    title: 'Studio96 Architekt Łukasz Rozkosz',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: '',
        name: 'Studio96-Architektura',
        content:
          'Studio96-Architektura Łukasz Rozkosz. Żory ul. Wodzisławska 96. Projektujemy domy jednorodzinne, obiekty użyteczności publicznej, wnętrza mieszkalne oraz usługowe. Zajmujemy się przebudowami i rozbudowami budynków. Modele 3D i wizualizacje',
      },
    ],
    link: [
      {
        hid: 'icon',
        rel: 'icon',
        type: 'image/x-icon',
        href: env.parsed.MEDIA_BASE_URL + 'assets/images/favicon.ico',
      },
    ],
  },

  env: {
    appUrl: env.parsed.APP_URL || 'http://localhost:3000',
    apiBaseUrl: env.parsed.API_BASE_URL || 'http://localhost:3001',
    mediaBaseUrl: env.parsed.MEDIA_BASE_URL || 'http://localhost:3001',
    tinyApiKey:
      env.parsed.TINY_API_KEY ||
      'rsmhwbe3jgi3mhezer07mck8qqlqpgaqw908gk3wsjslxsob',
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~plugins/vue-tiny', ssr: false },
    { src: '~plugins/vue-infinite-scroll', ssr: false },
    { src: '~plugins/vue-carousel', ssr: false },
    { src: '~plugins/vue-light-gallery', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: ['@/modules/generator'],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/sitemap',
  ],

  sitemap: {
    exclude: ['/admin/**', '/login/**', '/login', '/admin'],
    routes: async () => {
      const { data } = await axios.get(env.parsed.API_BASE_URL + '/seo')
      return data.map((item) => `/portfolio/gallery/${item}`)
    },
    generate: true,
    hostname: 'https://studio96-architektura.pl/',
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: env.parsed.API_BASE_URL,
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    vendor: ['@tinymce/tinymce-vue'],
    babel: {
      compact: true,
    },
  },

  auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/admin',
      user: '/admin',
    },

    strategies: {
      local: {
        token: {
          property: 'token',
          type: 'Bearer',
        },
        endpoints: {
          login: { url: '/login', method: 'post' },
          user: { url: '/me', method: 'get' },
        },
      },
    },
  },
}
