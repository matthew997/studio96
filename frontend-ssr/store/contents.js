'use strict'

export const defaultState = () => {
  return {
    content: {},
    footer: [],
  }
}

export const state = () => defaultState()

export const getters = {
  getContent: (state) => state.content,
  getFooter: (state) => state.footer,
}

export const mutations = {
  SET_DATA(state, content) {
    state.content = content
  },

  SET_FOOTER(state, footer) {
    state.footer = footer
  },
}

export const actions = {
  async getData({ commit }, { slug }) {
    const { data } = await this.$axios.get(`/contents/${slug}`)

    commit('SET_DATA', data)

    return data
  },

  async getFooterData({ commit }, slug) {
    const { data } = await this.$axios.get(`/contents/${slug}`)

    commit('SET_FOOTER', data)

    return data
  },

  async updateContent({ commit }, { slug, content }) {
    const { data } = await this.$axios.put(`/contents/${slug}`, {
      content,
    })

    commit('SET_DATA', data)

    return data
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
