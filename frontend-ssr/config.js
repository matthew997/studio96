module.exports = {
    mediaBaseUrl: process.env.mediaBaseUrl,
    tinyApiKey: process.env.tinyApiKey
};
